FROM openjdk:8-jdk-stretch
VOLUME /tmp
COPY target/spring-boot-hello-world.jar spring-boot-hello-world.jar
ENTRYPOINT ["java","-jar","spring-boot-hello-world.jar"]
